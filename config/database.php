<?php

class Database
{
	private $host = "ipv4.amirulbahriaziz.com";
	private $database_name = "concertdata";
	private $username = "amirulbahriaziz";
	private $password = "P@ssw0rd";

	public $conn;

	public function getConnection()
	{
		$this->conn = null;
		try {
			$this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->database_name, $this->username, $this->password);
			$this->conn->exec("set names utf8");
		} catch (PDOException $exception) {
			echo "Database could not be connected: " . $exception->getMessage();
		}
		return $this->conn;
	}
}
