<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../class/tickets.php';

$database = new Database();
$db = $database->getConnection();

$item = new Ticket($db);

$data = json_decode(file_get_contents("php://input"));

$item->f_name = $data->f_name;
$item->l_name = $data->l_name;
$item->category = $data->category;
$item->ticket_no = $data->ticket_no;
$item->total_price = $data->total_price;

if ($item->createTicket()) {
    echo 'Ticket created successfully.';
} else {
    echo 'Ticket could not be created.';
}
