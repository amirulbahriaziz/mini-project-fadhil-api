<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");

    include_once '../config/database.php';
    include_once '../class/tickets.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Ticket($db);

    $stmt = $items->getTickets();
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){

        $TicketArr = array();
        // $TicketArr["body"] = array();
        // $TicketArr["itemCount"] = $itemCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "ticket_id" => $ticket_id,
                "f_name" => $f_name,
                "l_name" => $l_name,
                "category" => $category,
                "ticket_no" => $ticket_no,
                "total_price" => $total_price
            );

            array_push($TicketArr, $e);
        }
        echo json_encode($TicketArr);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>