<?php
class Ticket
{

	// Connection
	private $conn;

	// Table
	private $db_table = "ticket";

	// Columns
	public $ticket_id;
	public $f_name;
	public $l_name;
	public $category;
	public $ticket_no;
	public $total_price;

	// Db connection
	public function __construct($db)
	{
		$this->conn = $db;
	}

	// GET ALL
	public function getTickets()
	{
		$sqlQuery = "SELECT ticket_id, f_name, l_name, category, ticket_no, total_price FROM " . $this->db_table . "";
		$stmt = $this->conn->prepare($sqlQuery);
		$stmt->execute();
		return $stmt;
	}

	// CREATE
	public function createTicket()
	{
		$sqlQuery = "INSERT INTO
                        " . $this->db_table . "
                    SET
                        f_name = :f_name,
                        l_name = :l_name,
                        category = :category,
                        ticket_no = :ticket_no,
                        total_price = :total_price";

		$stmt = $this->conn->prepare($sqlQuery);

		// sanitize
		$this->f_name = htmlspecialchars(strip_tags($this->f_name));
		$this->l_name = htmlspecialchars(strip_tags($this->l_name));
		$this->category = htmlspecialchars(strip_tags($this->category));
		$this->ticket_no = htmlspecialchars(strip_tags($this->ticket_no));
		$this->total_price = htmlspecialchars(strip_tags($this->total_price));

		// bind data
		$stmt->bindParam(":f_name", $this->f_name);
		$stmt->bindParam(":l_name", $this->l_name);
		$stmt->bindParam(":category", $this->category);
		$stmt->bindParam(":ticket_no", $this->ticket_no);
		$stmt->bindParam(":total_price", $this->total_price);

		if ($stmt->execute()) {
			return true;
		}
		return false;
	}

	// READ single
	public function getSingleTicket()
	{
		$sqlQuery = "SELECT
                        ticket_id,
                        f_name,
                        l_name,
                        category,
                        ticket_no,
                        total_price
                      FROM
                        " . $this->db_table . "
                    WHERE
                       ticket_id = ?
                    LIMIT 0,1";

		$stmt = $this->conn->prepare($sqlQuery);

		$stmt->bindParam(1, $this->ticket_id);

		$stmt->execute();

		$dataRow = $stmt->fetch(PDO::FETCH_ASSOC);

		$this->f_name = $dataRow['f_name'];
		$this->l_name = $dataRow['l_name'];
		$this->category = $dataRow['category'];
		$this->ticket_no = $dataRow['ticket_no'];
		$this->total_price = $dataRow['total_price'];
	}

	// UPDATE
	public function updateTicket()
	{
		$sqlQuery = "UPDATE
                        " . $this->db_table . "
                    SET
                        f_name = :f_name,
                        l_name = :l_name,
                        category = :category,
                        ticket_no = :ticket_no,
                        total_price = :total_price
                    WHERE
                        ticket_id = :ticket_id";

		$stmt = $this->conn->prepare($sqlQuery);

		$this->f_name = htmlspecialchars(strip_tags($this->f_name));
		$this->l_name = htmlspecialchars(strip_tags($this->l_name));
		$this->category = htmlspecialchars(strip_tags($this->category));
		$this->ticket_no = htmlspecialchars(strip_tags($this->ticket_no));
		$this->total_price = htmlspecialchars(strip_tags($this->total_price));
		$this->ticket_id = htmlspecialchars(strip_tags($this->ticket_id));

		// bind data
		$stmt->bindParam(":f_name", $this->f_name);
		$stmt->bindParam(":l_name", $this->l_name);
		$stmt->bindParam(":category", $this->category);
		$stmt->bindParam(":ticket_no", $this->ticket_no);
		$stmt->bindParam(":total_price", $this->total_price);
		$stmt->bindParam(":ticket_id", $this->ticket_id);

		if ($stmt->execute()) {
			return true;
		}
		return false;
	}

	// DELETE
	function deleteTicket()
	{
		$sqlQuery = "DELETE FROM " . $this->db_table . " WHERE ticket_id = ?";
		$stmt = $this->conn->prepare($sqlQuery);

		$this->ticket_id = htmlspecialchars(strip_tags($this->ticket_id));

		$stmt->bindParam(1, $this->ticket_id);

		if ($stmt->execute()) {
			return true;
		}
		return false;
	}
}
