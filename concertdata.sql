-- --------------------------------------------------------
-- Host:                         ipv4.amirulbahriaziz.com
-- Server version:               8.0.26-0ubuntu0.20.04.2 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for concertdata
CREATE DATABASE IF NOT EXISTS `concertdata` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `concertdata`;

-- Dumping structure for table concertdata.ticket
CREATE TABLE IF NOT EXISTS `ticket` (
  `ticket_id` int unsigned NOT NULL AUTO_INCREMENT,
  `f_name` varchar(20) NOT NULL DEFAULT '0',
  `l_name` varchar(20) NOT NULL DEFAULT '0',
  `category` varchar(20) NOT NULL DEFAULT '0',
  `ticket_no` int NOT NULL DEFAULT '0',
  `total_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`ticket_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table concertdata.ticket: ~1 rows (approximately)
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` (`ticket_id`, `f_name`, `l_name`, `category`, `ticket_no`, `total_price`) VALUES
	(1, 'Amirul', 'Aziz', 'Adults', 2, 500.00),
	(2, 'Jemah', 'Kurom', 'Childs', 1, 100.00),
	(3, 'Amirul', 'Aziz', 'Accounting', 3, 200.00),
	(4, 'asd', 'qwe', '100.00', 2, 6.00),
	(5, 'gggg', 'rrrr', '20.00', 1, 7.00),
	(6, 'qwe', 'vfsd', '', 1, 1.00);
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
